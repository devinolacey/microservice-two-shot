from django.urls import path
from .api_views import api_list_shoes, api_show_shoe, delete_binVO, list_binVO

urlpatterns = [
    path('shoes/', api_list_shoes, name='api_list_shoes'),
    path('bins/<int:bin_vo_id>/', delete_binVO, name='delete_binVO'),
    path('bins/', list_binVO, name='list_binVO'),
    path('bins/<int:bin_vo_id>/shoes/', api_list_shoes, name='api_list_shoes'),
    path('shoes/<int:pk>/', api_show_shoe, name='api_show_shoe'),
    
]