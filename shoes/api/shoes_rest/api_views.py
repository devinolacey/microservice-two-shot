from django.shortcuts import render
import json
from common.json import ModelEncoder
from .models import Shoe, BinVo
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse


# Create your views here.
class BinVoDetail(ModelEncoder):
    model = BinVo
    properties = ['name', 'import_href', 'id']

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ['shoe_name', 'id']
    
    def get_extra_data(self, o):
        return {'bin': o.bin.name,
                'bin_id': o.bin.import_href}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'shoe_name',
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'id',
        'bin'
        ]
    encoders = {
        "bin": BinVoDetail(),
        }
    
    def get_extra_data(self, o):
        return {'bin_name': o.bin.name,
                'bin_id': o.bin.import_href}

@require_http_methods(['GET', 'POST'])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == 'GET':
        print(bin_vo_id)

        if(bin_vo_id == None):
            shoes = Shoe.objects.all()
        else:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
            print(shoes)
    
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )

    else: # request.method == 'POST'
        content = json.loads(request.body)
        print(f'content {content}')

        try:
            bin_href = content["bin"]
            print(f'bin href {bin_href}')
            bin = BinVo.objects.get(import_href=bin_href)
            content['bin'] = bin
        except BinVo.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid bin you dummy'},
                status=400
            )
        
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    
@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_shoe(request, pk):
    if request.method == 'GET':

        try:
            shoe = Shoe.objects.get(id=pk)
            print(f'------------------------------{shoe}')
        except Shoe.DoesNotExist:
            return JsonResponse({'message': 'Bad Id .... your so useless'})


        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    
    elif request.method == 'DELETE':
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({'response': count > 0})
    
    else: # request.method == 'PUT'
        content = json.loads(request.body)
        print(f'content {content}')
        try:
            bin_content = content.get('bin')
            print(f'bin_content {bin_content}')
            bin = BinVo.objects.get(import_href=bin_content['import_href'])
            content['bin'] = bin
        except BinVo.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin GET BETTER"},
                status=400,
            )
        
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods('DELETE')
def delete_binVO(request, bin_vo_id):
    if request.method == 'DELETE':
        count, _ = BinVo.objects.filter(id=bin_vo_id).delete()
        return JsonResponse({'response': count > 0})
    
@require_http_methods(['GET'])
def list_binVO(request):
    if request.method == 'GET':

        voBins = BinVo.objects.all()
        return JsonResponse(
            {'voBins': voBins},
            encoder=BinVoDetail,
            safe=False,
        )