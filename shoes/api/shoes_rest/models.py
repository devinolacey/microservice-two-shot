from django.db import models
from django.urls import reverse


# Create your models here.

class BinVo(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name




class Shoe(models.Model):
    shoe_name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200, blank=True)
    model_name = models.CharField(max_length=200, blank=True)
    color = models.CharField(max_length=50, blank=True)
    picture_url = models.URLField(blank=True)
    bin = models.ForeignKey(
        BinVo,
        related_name="shoe",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.shoe_name
    
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})