import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVo


def get_bin():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    hrefs = []
    for bin in content['bins']:
        BinVo.objects.update_or_create(
            import_href = bin['href'],
            name = bin['closet_name'],
        )
        print(bin['href'])
        hrefs.append(bin['href'])
    # below was my implementation to delete the value objects from the database if the bin is deleted

        
        
    binVo = BinVo.objects.all()
    for bin in binVo:
        if bin.import_href not in hrefs:
            BinVo.objects.filter(import_href=bin.import_href).delete()
        # print(bin.import_href)

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bin()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
