import React, { useEffect, useState } from "react";
import { useDefaultState } from "./Helper";
import { useNavigate } from "react-router-dom";

function ShoeForm ({bins, getShoes , getBins}) {
    // console.log(bins)
    // const shoeName = useDefaultState();
    // const manufacturer = useDefaultState();
    // const modelName = useDefaultState();
    // const color = useDefaultState();
    // const pictureUrl = useDefaultState();
    // const bin = useDefaultState();
    const navigate = useNavigate();

    const [formData, setFormData] = useState({
        shoe_name:'',
        manufacturer:'',
        model_name:'',
        color:'',
        picture_url:'',
        bin:'',
    })

    const handleSubmit = async (e) => {
        e.preventDefault();

        // data.shoe_name = shoeName;
        // data.manufacturer = manufacturer;
        // data.model_name = modelName;
        // data.color = color;
        // data.picture_url = pictureUrl;
        // data.bin = bin;


        const shoeUrl = `http://localhost:8080/api/shoes/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            // console.log(newShoe);

            setFormData({
                shoe_name:'',
                manufacturer:'',
                model_name:'',
                color:'',
                picture_url:'',
                bin:''
            })
            getShoes();
            navigate('/shoes/');


            // shoeName.reset()
            // manufacturer.reset()
            // modelName.reset()
            // color.reset()
            // pictureUrl.reset()
            // bin.reset()
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name
        const value = e.target.value
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    useEffect(() => {
        getBins();
    }, [])

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.shoe_name} placeholder="Shoe Name" required type="text" name="shoe_name" id="shoe_name" className="form-control"/>
                        <label htmlFor="shoe_name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.model_name} placeholder="Model" required type="text" name="model_name" id="model_name" className="form-control"/>
                        <label htmlFor="model_name">Model</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture"  type="url" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.bin} required id="bin" name= "bin" className="form-select">
                            <option value="">Pick a Bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    )
}

export default ShoeForm;
