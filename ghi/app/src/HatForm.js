import React, { useState } from 'react';

function HatForm({ locations }) {
  // const [locations, setLocations] = useState([])
  //takes locations as prop
  //initalize state with my form values
    const [hatData, setHatData] = useState({
      hat_name: '',
      fabric: '',
      style_name: '',
      color: '',
      url: '',
      location: '',
      })

    // useState = hook
    // formData is state variable, setFormData is the setter/getter


    const handleSubmit= async (event) => {
      event.preventDefault();

    //function is handling the actual form submission.

    //Extrast location from form Data
      // const { locations } = hatData;

    //Create URL for API endpoint
    //Where are the hats coming from?
      const hatUrl = 'http://localhost:8090/api/hats/';

      const fetchConfig  = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(hatData),
      };

      //Make post request

      const createHatResponse = fetch(hatUrl, fetchConfig);
      if (createHatResponse.ok) {
        const newHat = createHatResponse.json();
        console.log(newHat);

        setHatData({
          hat_name: '',
          fabric: '',
          style_name: '',
          color: '',
          url: '',
          location: ''})

        //update the hat form data
        }}
      const handleChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setHatData({
        ...hatData,
        [inputName]: value,
      })
    }

    return(
        <div>
            <p>
                Want to Create a New Hat!?
            </p>
            <form onSubmit={handleSubmit} id="create-hat-form">
                <label htmlFor="hatName" className="form-label">
                   Hat Name:
                   </label>
                <input
                name="hatName" id="hatName" value={hatData.hat_name}
                onChange= {handleChange} type="text"></input>


                <label htmlFor="fabric" className="form-label">
                   Fabric:
                   </label>
                <input
                name="fabric" id="fabric" value={hatData.fabric}
                onChange= {handleChange} type="text"></input>

                <label htmlFor="style_name" className="form-label">
                   Style:
                   </label>
                <input
                name="style_name" id="style_name" value={hatData.style_name}
                onChange= {handleChange} type="text"></input>

                <label htmlFor="color" className="form-label">
                   Color:
                   </label>
                <input
                name="color" id="color" value={hatData.color}
                onChange= {handleChange} type="text"></input>

                <label htmlFor="url" className="form-label">
                   Picture:
                   </label>
                <input
                name="url" id="url" value={hatData.url}
                onChange= {handleChange} type="text"></input>

                <label htmlFor="location" className="form-label">Location:</label>
                <select
                  name="location"
                  id="location"
                  value={hatData.location}
                  onChange={handleChange}>
                <option value="">Pick a location</option>
                  {locations.map((location) => (
                    <option key={location.id} value={location.import_href}>
                      {location.name}
                    </option>
                  ))}
                </select>



                <button type="submit">Create Hat</button>
            </form>
        </div>
    )
}
export default HatForm;
