import React, { useState } from "react";
import { useDefaultState } from "./Helper";
import { useNavigate } from "react-router-dom";

function BinForm ({getShoes}) {

    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        closet_name: '',
        bin_number: '',
        bin_size: ''
    })

    const handleSubmit = async (e) => {
        e.preventDefault();


        const binUrl = `http://localhost:8100/api/bins/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            // console.log(newBin);

            setFormData({
                closet_name: '',
                bin_number: '',
                bin_size: ''
            })
            getShoes();
            navigate('/shoes/');

        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name
        const value = e.target.value
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Bin</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.closet_name} placeholder="Closet Name" required type="text" name="closet_name" id="closet_name" className="form-control"/>
                        <label htmlFor="closet_name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.bin_number} placeholder="Bin Number" required type="number" name="bin_number" id="bin_number" className="form-control"/>
                        <label htmlFor="bin_number">Bin number</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.bin_size} placeholder="Bin Size" required type="number" name="bin_size" id="bin_size" className="form-control"/>
                        <label htmlFor="bin_size">Bin size </label> {/*i debated removing this option and preset to infinite has i do not see myself implementing something to enforce this size limiter but decided to leave this option here */}
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    )
}

export default BinForm;