import { useParams, useNavigate, NavLink, Navigate } from "react-router-dom";
import { useState, useEffect } from "react";

function ShoeDetails({getShoes}) {
    const navigate = useNavigate();

    const handleDelete = async (e) => {
        e.preventDefault();

        const deleteUrl = `http://localhost:8080/api/shoes/${shoe.id}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(deleteUrl, fetchConfig);
        if (response.ok) {
            getShoes()
            navigate('/shoes/');
        } 
    }

    const params = useParams();

    async function getShoe(id) {
        const response = await fetch(`http://localhost:8080/api/shoes/${id}`);
        const shoe = await response.json();
        setShoe(shoe);
    }

    useEffect(() => {
        getShoe(Number(params.id));
    }, [])

    const [shoe, setShoe] = useState([]);

    if (shoe === undefined) {
        return null;
    }
    // console.log(shoe);
    const handleRedirect = (e) => {
        e.preventDefault();
        navigate(`/bins${shoe.bin_id}`)
    }

    

    return (
        <div className="card" style={{width: "25rem", padding: '20px', border: '3px solid rgba(0,0,0,.5)', margin: 'auto', marginTop: '50px'}}>
            <img src={shoe.picture_url} className="card-img-top" alt='...' onError={(e) => (e.target.src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpDLZIHwU1zt9Ifh3GWK4LR-4iH1_CgWuTECFG9AMHcdyCmb9wFBGiq4811efKSkdQR5s&usqp=CAU")} />
            <div className="card-body">
                <h5 className="card-title">{shoe.shoe_name}</h5>
            </div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item">Manufacturer: {shoe.manufacturer}</li>
                <li className="list-group-item">Model: {shoe.model_name}</li>
                <li className="list-group-item">Color: {shoe.color}</li>
                <li className="list-group-item">Bin: {shoe.bin_name}</li>
            </ul>
            {/* <div class="btn-group" role="group" aria-label="Basic example"> */}
            <div style={{display:'flex', justifyContent:'space-evenly'}}>
                <button className="btn btn-primary" style={{width: 'fit-content', marginTop: '11px'}} onClick={handleRedirect}>View Bin</button>
                <button type="button" className="btn btn-danger" style={{marginTop: '11px'}} onClick={handleDelete}>Delete</button>
            </div>
        </div>
    )
}

export default ShoeDetails