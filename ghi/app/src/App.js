import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeDetails from './ShoeDetails';
import ShoeForm from './ShoeForm'
import BinDetails from './BinDetails';
import BinForm from './BinForm';
import HatList from './HatList';
import HatForm from './HatForm';
import HatListDetail from './HatListDetail'
import { useEffect, useState } from 'react';


function App() {
  const [shoes, setShoes] = useState([]);
  const [bins, setBins] = useState([]);
  const [hats, setHats] = useState([])
  const [locations, setLocations] = useState([]);

  async function getShoesies() {
    const response = await fetch('http://localhost:8080/api/shoes/')

    if (response.ok) {
      const {shoes} = await response.json();
      setShoes(shoes);
    } else {
        console.log('An error occurred while fetching the data')
    }
  }

  async function getBins() {
    const response = await fetch('http://localhost:8100/api/bins/')

    if (response.ok) {
      const {bins} = await response.json();
      setBins(bins);
      // console.log(bins);
    } else {
      console.log('An error occurred while fetching the data')
    }
  }

  async function getHats () {
    const response = await fetch('http://localhost:8090/api/hats/')

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }  else {
      console.log("An error occurred while fetching hat data")
    }

  }


  async function getLocations() {
    const response = await fetch('http://localhost:8090/api/locations/')

    if (response.ok) {
      const {locations} = await response.json();
      setLocations(locations);
    } else {
      console.log('An error occurred while fetching the data')
    }
  }












































useEffect(() => {
  getShoesies();
  getBins();
  getHats();
  getLocations();
}, [])

if (shoes === undefined || hats === undefined) {
  return null;
}

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoes} getBins={getBins} />} />
            <Route path=':id' element={<ShoeDetails getShoes={getShoesies} />} />
            <Route path='new' element={<ShoeForm bins={bins} getShoes={getShoesies} getBins={getBins} />} />
          </Route>
          <Route path='bins'>
            <Route path='api/bins/:bin_id' element={<BinDetails getBins={getBins} shoes={shoes} getShoes={getShoesies}/>} />
            <Route path='new' element={<BinForm getShoes={getShoesies} />} />
          </Route>
          <Route path="Hats" >
            <Route index element={<HatList hats={hats} />} />
            <Route path=":id" element={<HatListDetail hats={hats}/>} />
            <Route path="NewHat" element={<HatForm/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
