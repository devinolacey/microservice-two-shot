import { useParams, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";

function BinDetails({getBins, shoes, getShoes}) {
    const navigate = useNavigate();
    const [voBins, setVoBins] = useState([]);


    async function getVoBins() {
        const response = await fetch('http://localhost:8080/api/bins/')
        
        if (response.ok) {
            const {voBins} = await response.json();
            setVoBins(voBins);
        } else {
            console.log('Failed to fetch')
        }
    }


    const handleDelete = async (e) => {
        e.preventDefault();

        const deleteBin = `http://localhost:8100${bin.href}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const binResponse = await fetch(deleteBin, fetchConfig);

        
        console.log(voBinId)
        const deleteBinVo = `http://localhost:8080/api/bins/${voBinId}`
        const fetchConfig2 = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const binVoResponse = await fetch(deleteBinVo, fetchConfig2)



        if (binResponse.ok && binVoResponse.ok) {
            getBins();
            getShoes();
            navigate('/shoes/');
        } 
    }

    const params = useParams();
    // console.log(params.bin_id);

    async function getBin(id) {
        const response = await fetch(`http://localhost:8100/api/bins/${id}`);
        const bin = await response.json();
        setBin(bin);
        // console.log(bin.id)
        // console.log(bin.href)
    }

    useEffect(() => {
        getBin(Number(params.bin_id));
        getVoBins();
    }, [])

    const [bin, setBin] = useState([]);

    if (bin === undefined) {
        return null;
    }

    const inBin = [];
    shoes.map(shoe => {
        if (shoe.bin_id === bin.href) {
            inBin.push(shoe);
        }
    })
    
    let voBinId
    voBins.map(voBin => {
        if (bin.href === voBin.import_href) {
            voBinId = voBin.id
        }
    })
    
    // console.log(voBinId)
    

    return (
        <div className="card" style={{width: "25rem", padding: '20px', border: '3px solid rgba(0,0,0,.5)', margin: 'auto', marginTop: '50px'}}>
            <div className="card-body">
                <h5 className="card-title">{bin.closet_name}</h5>
            </div>
             <ul className="list-group list-group-flush">
            {inBin.map(shoe => {
                return(
                <li key={shoe.id} className="list-group-item">
                     <button type="button" className="btn btn-link" style={{textDecoration: 'auto'}} onClick={() => (navigate(`/shoes/${shoe.id}`))}>{shoe.shoe_name}</button>
                </li>
            )})}
            </ul>
            {/* <div style={{display:'flex', justifyContent:'space-evenly'}}> */}
                <button type="button" className="btn btn-danger" style={{marginTop: '11px'}} onClick={handleDelete}>Delete</button>
            {/* </div> */}
        </div>
    )
}

export default BinDetails