import { useParams } from "react-router-dom";


function getHat(id, hats) {
 return hats.find(hat => hat.id === id);
}

function HatListDetail( {hats} ) {
    const params = useParams();
    const hat = getHat(Number(params.id), hats)

    return(
        <div>
        <p>{hat.hat_name}</p>
        <p>Fabric: {hat.fabric}</p>
        <p>Style: {hat.style_name}</p>
        <p>Color: {hat.color}</p>
        </div>
    )

}


export default HatListDetail;
