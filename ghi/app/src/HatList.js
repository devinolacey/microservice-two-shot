import React from 'react';
import { NavLink } from 'react-router-dom'

function HatList ({ hats, getHats }) {

  const handleDelete = async (hat) => {
    const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`
    const fetchConfig = {
        method: 'delete'
    }
    const hatResponse = await fetch(hatUrl, fetchConfig)
    if (hatResponse.ok) {
        getHats()
    }
  }


  return (
    <div>
    <NavLink to="/hats/new" className="btn btn-primary">
      Create New Hat
    </NavLink>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Delete</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
            console.log(hat);
          return (
            <tr key={hat.id}>
                <td>
                <NavLink className="nav-link" to={`${hat.id}`} >{hat.hat_name} </NavLink>
                </td>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td><img src={hat.url} className= "fit-picture"/></td>
              <td>
                <button type="button" id={hat.id} onClick={() => handleDelete(hat)} className="btn btn-warning">
                                    Delete
                                </button>
              </td>
              <td>
              { hat.location }
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );
}

export default HatList;
