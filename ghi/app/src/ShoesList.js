import React, { useEffect } from "react";
import { NavLink, useNavigate} from "react-router-dom";

function ShoesList ({shoes, getBins, voBins}) {
    // console.log(shoes)
    const navigate = useNavigate();
    // console.log(voBins)

    // useEffect(() => {
    //     getShoes();
    // })

    return (
    <div>
        <table className="table table-striped table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Bin</th>
            </tr>
            </thead>
            <tbody>
            {shoes.map(shoe => {
                return (
                <tr key={shoe.id}>
                    <td>
                        <NavLink className="nav-link" to={`${shoe.id}`} style={{width: 'fit-content'}} >{shoe.shoe_name}</NavLink>
                    </td>
                    <td>
                        <button type="button" className="btn btn-link" style={{textDecoration: 'auto'}} onClick={() => (navigate(`/bins${shoe.bin_id}`))}>{shoe.bin}</button>
                    </td>
                </tr>
                );
            })}
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <div style={{display:'flex', justifyContent:'flex-end'}}>
            <NavLink  to='new' style={{width: 'fit-content'}}><button className="btn btn-primary">Add a shoe</button></NavLink>
            <button className="btn btn-primary" style={{marginLeft: '20px'}} onClick={() => {navigate('/bins/new'); getBins()}}>Add a bin</button>
        </div>
    </div>
    )
}

export default ShoesList
