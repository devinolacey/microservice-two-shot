import { useState } from "react";

export function useDefaultState() {
    const [state, setState] = useState('')

    function handleChange(e) {
        e.preventDefault()
        setState(e.target.value)
    }

    function reset() {
        setState('')
    }

    return {'state': state, 'handleChange': handleChange, 'reset': reset}}