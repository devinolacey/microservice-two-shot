from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Hat(models.Model):
    hat_name = models.CharField(max_length=30)
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    url = models.URLField(blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_hat_details", kwargs={"pk": self.pk})

    def __str__(self):
        return self.hat_name
