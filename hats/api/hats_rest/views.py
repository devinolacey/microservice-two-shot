from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = ["hat_name",
                  "id",
                  "fabric",
                  "style_name",
                  "color",
                  "url",
                  ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["hat_name",
                  "fabric",
                  "style_name",
                  "color",
                  "url",
                  "id",
                  "location"
                  ]
    encoders = {"location": LocationVOEncoder(),}


@require_http_methods(["GET", "POST"])
def api_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
            safe=False,
        )
    else: #POST

        content = json.loads(request.body)
        print(content)


        try:

            location = content['location']
            print(location)
            location = LocationVO.objects.get(import_href=location)
            content['location'] = location
        except (LocationVO.DoesNotExist):
            return JsonResponse(
                {'message': 'Invalid location data or LocationVO does not exist'},
                status=400
            )

        hat = Hat.objects.create(**content)

        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat_details(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Not here"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
            content = json.loads(request.body)
            try:

                hat = Hat.objects.get(id=pk)

                props = ["hat_name",
                    "fabric",
                    "style_name",
                    "color",
                    "url",
                    ]
                for prop in props:
                    if prop in content:
                        setattr(hat, prop, content[prop])
                hat.save()
                return JsonResponse(
                    hat,
                    encoder=HatDetailEncoder,
                    safe=False,
                )
            except Hat.DoesNotExist:
                response = JsonResponse({"message": "Does not exist"})
                response.status_code = 404
                return response

@require_http_methods([ "GET"])
def api_locations(request):
        locations = LocationVO.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationVOEncoder,
            safe=False,
        )







# """
#     Single-object API for the Location resource.

#     GET:
#     Returns the information for a Location resource based
#     on the value of pk
#     {
#         "id": database id for the location,
#         "closet_name": location's closet name,
#         "section_number": the number of the wardrobe section,
#         "shelf_number": the number of the shelf,
#         "href": URL to the location,
#     }

#     PUT:
#     Updates the information for a Location resource based
#     on the value of the pk
#     {
#         "closet_name": location's closet name,
#         "section_number": the number of the wardrobe section,
#         "shelf_number": the number of the shelf,
#     }

#     DELETE:
#     Removes the location resource from the application
#     """
