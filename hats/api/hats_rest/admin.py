from django.contrib import admin
from .models import Hat

@admin.register(Hat)
class Hat(admin.ModelAdmin):
    pass
