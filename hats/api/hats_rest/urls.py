from django.contrib import admin
from django.urls import path
from .views import api_hats, api_hat_details, api_locations

urlpatterns = [
    path('hats/', api_hats, name ="api_hats"),
    path('hats/<int:pk>/', api_hat_details, name='api_hat_details'),
    path('locations/', api_locations, name="api_show_locations"),
]
